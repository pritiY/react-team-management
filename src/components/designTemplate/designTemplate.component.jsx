import React, { Component } from 'react';
import './designTemplate.component.scss';

class DesignTemplate extends Component {
    state = {
        template: this.props.template,
        templateImage: "https://media.giphy.com/media/W2K6R2Ec4Hy4o/giphy.gif"
    }
    previewTemplate = () => {
        console.log('okay ', this.state.template.templateImage);
        this.setState(state => ({
            templateImage: this.state.template.image
        }));
        //this.setState(state => ({templateImage: this.state.template.templateImage}));
    }
    render() {
        return (
            <>
                <div className="shadow">
                    <div className="col-md-12">
                        <div className="row">
                            <div className="col-md-12 mt-2 mb-3">
                                <button type="button" className="close" onClick={this.props.onClose} aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <div className="input-group mb-2">
                                        <div className="input-group-prepend">
                                            <div className="input-group-text bg-light">Header Text</div>
                                        </div>
                                        <input type="text" className="form-control" id="inlineFormInputGroup" placeholder="Header Text" />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="input-group mb-2">
                                        <div className="input-group-prepend">
                                            <div className="input-group-text bg-light">Body Text</div>
                                        </div>
                                        <input type="text" className="form-control" id="inlineFormInputGroup" placeholder="Body Text" />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="input-group mb-2">
                                        <div className="input-group-prepend">
                                            <div className="input-group-text bg-light">Footer Text</div>
                                        </div>
                                        <input type="text" className="form-control" id="inlineFormInputGroup" placeholder="Footer Text" />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="input-group mb-2">
                                        <div className="input-group-prepend">
                                            <div className="input-group-text bg-light">Image URL</div>
                                        </div>
                                        <input type="text" className="form-control" id="inlineFormInputGroup" placeholder="Image URL" />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="input-group mb-2">
                                        <div className="input-group-prepend">
                                            <div className="input-group-text bg-light">Button Text</div>
                                        </div>
                                        <input type="text" className="form-control" id="inlineFormInputGroup" placeholder="Button Text" />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="input-group mb-2">
                                        <div className="input-group-prepend">
                                            <div className="input-group-text bg-light">Button Link</div>
                                        </div>
                                        <input type="text" className="form-control" id="inlineFormInputGroup" placeholder="Button Link" />
                                    </div>
                                </div>
                                <div className="form-row">
                                    <div className="form-group col-md-10">
                                        <div className="input-group">
                                            <div className="input-group-prepend">
                                                <div className="input-group-text bg-light">Color</div>
                                            </div>
                                            <input type="color" className="form-control" id="inlineFormInputGroup" placeholder="Color" />
                                        </div>
                                    </div>
                                    <div className="form-group col-md-2">
                                        <button className="btn btn-outline-primary" onClick={this.previewTemplate}>Preview</button>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <img src={this.state.templateImage} className="App-logo" alt="logo" />
                            </div>
                        </div>
                    </div>
                    <nav className="navbar col-md-12 sticky-bottom navbar-expand-lg navbar-light" style={{ backgroundColor: "#e3f2fd" }}>

                        <span className="navbar-brand" >{this.state.template.name}</span>

                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav mr-auto">
                            </ul>
                            <div className="form-inline my-2 my-lg-0">
                                <button className="btn btn-outline-primary" onClick={this.previewTemplate}>Preview</button>
                                <button className="btn btn-outline-success ml-2">Save</button>
                            </div>
                        </div>
                    </nav>
                </div>
            </>
        );
    }
}

export default DesignTemplate;
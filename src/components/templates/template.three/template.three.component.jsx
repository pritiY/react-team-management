import React, { Component } from 'react';
import templateImage from '../../../assets/Template_3.png';

export default class TemplateThree extends Component {
    render() {
        return (
            <div><img src={templateImage} className="App-logo" alt="logo" /></div>
        );
    }
}
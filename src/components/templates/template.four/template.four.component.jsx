import React, { Component } from 'react';
import templateImage from '../../../assets/Template_4.png';

export default class TemplateFour extends Component {
    render() {
        return (
            <div><img src={templateImage} className="App-logo" alt="logo" /></div>
        );
    }
}
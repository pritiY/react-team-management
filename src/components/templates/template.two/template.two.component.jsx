import React, { Component } from 'react';
import templateImage from '../../../assets/Template_2.png';

export default class TemplateTwo extends Component {
    render() {
        return (
            <div><img src={templateImage} className="App-logo" alt="logo" /></div>
        );
    }
}
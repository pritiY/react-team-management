import React, { Component } from 'react';
import templateImage from '../../../assets/Template_1.png';

export default class TemplateOne extends Component {
    render() {
        return (
            <div><img src={templateImage} className="App-logo" alt="logo" /></div>
        );
    }
}
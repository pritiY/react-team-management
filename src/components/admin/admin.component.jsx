import React, { Component } from 'react';
import './admin.component.scss';
import DesignTemplate from '../designTemplate/designTemplate.component';

class Admin extends Component {
    state = {
        templates: [{
            name: 'Template 1', id: 'Template1',
            image: 'https://screenshots.webflow.com/sites/5d43138a62513bf6a05d9974/20200107163412_df505f292986e50e410625c083ace65e.png'
        },
        {
            name: 'Template 2', id: 'Template2',
            image: 'https://screenshots.webflow.com/sites/5c1ddc74fd28a7bb5de83a1c/20181226115325_69665a48e5b1f97a329420a7aa1e464a.png'
        },
        {
            name: 'Template 3', id: 'Template3',
            image: 'https://screenshots.webflow.com/sites/5b32655069229148933e1636/20180628095543_52d95b421c7c967973f1e68e772705a6.png'
        },
        {
            name: 'Template 4', id: 'Template4',
            image: 'https://uploads-ssl.webflow.com/5d28f5b286b66bc74fb8ea7b/5d9a30ab4aefbe6e7696f0eb_Screen%20Shot%202019-10-06%20at%2019.20.52.png'
        }, {
            name: 'Template 5', id: 'Template5',
            image: 'https://uploads-ssl.webflow.com/55ce74221f74ecaa28644c33/201802282222.png'
        },
        {
            name: 'Template 6', id: 'Template6',
            image: 'https://daks2k3a4ib2z.cloudfront.net/5757450dbe901fd644024d5e/58cf32a94c05612c3e2ef91f_tumb-wb.png'
        },
        {
            name: 'Template 7', id: 'Template7',
            image: 'https://daks2k3a4ib2z.cloudfront.net/59e8d196cd358f00013e466f/59e9fa76c3e5160001eda929_webflow%20thumb.png'
        },
        {
            name: 'Template 8', id: 'Template8',
            image: 'https://uploads-ssl.webflow.com/5a81614438edac0001884535/5a81614438edac00018845c4_cloud-nine-thumbnail.png'
        }
        ],
        selectedTemplate: null,
        activeTab: "MY_TEMP"
    };
    selectTemplate = template => {
        console.log('clicked');
        this.setState(state => ({
            selectedTemplate: template
        }));
    }
    switchTab = tabName => {
        this.setState(state => ({
            activeTab: tabName
        }));
    }
    deSelectTemplate = () => {
        console.log('Test');
        this.setState(state => ({
            selectedTemplate: null
        }));
    }
    render() {
        return (<>

            <div className="container">
                <div className="row">
                    {this.state.selectedTemplate == null && <nav className="navbar col-md-12">

                        <ul className="nav nav-tabs col-md-12">
                            <li className="nav-item" onClick={() => this.switchTab("MY_TEMP")}>
                                <span className={this.state.activeTab === "MY_TEMP" ? "nav-link active" : "nav-link"}>My Templates</span>
                            </li>
                            <li className="nav-item"  onClick={() => this.switchTab("NEW")}>
                                <span className={this.state.activeTab === "NEW" ? "nav-link active" : "nav-link"}>New Templates</span>
                            </li>
                        </ul>
                    </nav>}
                    {this.state.selectedTemplate == null && this.state.templates.map(template => <div className="col-md-3" key={template.id}>
                        <div className="shadow">
                            <div className="card" onClick={() => this.selectTemplate(template)}>
                                <img src={template.image} className="card-img-top" alt="logo" />
                                <div className="card-footer bg-transparent"><label>{template.name}</label></div>
                            </div>
                        </div>
                    </div>)}
                    {this.state.selectedTemplate != null && <div className="col-md-12"><DesignTemplate onClose={this.deSelectTemplate} template={this.state.selectedTemplate} /></div>}
                </div>
                <br></br>
            </div>

        </>);
    }
}

export default Admin;
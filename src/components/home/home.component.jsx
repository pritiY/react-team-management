import React, { Component } from 'react';
import logo from '../../team-logo.jpg';

export default class Home extends Component {
    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <hr></hr>
                    <p>
                        We Are The Team Tortoise.
                    </p>
                    <hr></hr>
                </header>
            </div>
        );
    }
}
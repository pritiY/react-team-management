import React, { Component } from 'react';
import './navBar.component.scss';
import Nav from 'react-bootstrap/Nav';

export default class NavBar extends Component {
    render() {
        return (
            <>
                <header id="site-header">
                    <div className="container">
                        <div className="row no-gutters">
                            <nav className="col-12 center">
                                <a href="https://optus.com.au/">
                                    <img
                                        src="http://image.email.optus.com.au/lib/fe9412727565007a72/m/4/Optus-logo-desktop.png"
                                        alt="Optus" />
                                </a>
                            </nav>
                        </div>
                    </div>
                </header>
                <Nav fill variant="tabs" defaultActiveKey="/home" className="container mb-4">
                    <Nav.Item className="border-right border-top">
                        <Nav.Link href="/template/one">MOBILE DEALS</Nav.Link>
                    </Nav.Item>
                    <Nav.Item className="border-right border-top">
                        <Nav.Link href="/template/two">IPAD, TABLET &amp; WATCHES</Nav.Link>
                    </Nav.Item>
                    <Nav.Item className="border-right border-top">
                        <Nav.Link href="/template/three">PREPAID DEALS</Nav.Link>
                    </Nav.Item>
                    <Nav.Item className="border-right border-top">
                        <Nav.Link href="/template/four">
                            BROADBAND BUNDLES
                        </Nav.Link>
                    </Nav.Item>
                </Nav>
            </>
        );
    }

}
import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import './App.css';
import NavBar from './components/navBar/navBar.component';
import Home from './components/home/home.component'
import Admin from './components/admin/admin.component'
import OffersHome from './components/offers-home/offers-home.component';
import TemplateOne from './components/templates/template.one/template.one.component'
import TemplateTwo from './components/templates/template.two/template.two.component'
import TemplateThree from './components/templates/template.three/template.three.component'

function App() {
  return (
    <div className="App">
      <NavBar />
      <BrowserRouter>
        <Switch>
          <Route path='/offers' exact component={OffersHome} />
          <Route path='/admin' exact component={Admin} />
          <Route path='/template/one' exact component={TemplateOne} />
          <Route path='/template/two' exact component={TemplateTwo} />
          <Route path='/template/three' exact component={TemplateThree} />
          <Route path='**' exact component={Home} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
